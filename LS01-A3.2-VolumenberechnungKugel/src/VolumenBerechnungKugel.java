import java.util.Scanner;

public class VolumenBerechnungKugel {

	public static void main(String[] args) {
		double r;
		double ergebnisVolumenKugel;
		
		//Eingabe
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie den Radius ein: ");
		r = tastatur.nextDouble();
		
		//Verarbeitung
		ergebnisVolumenKugel = berechnungVolumenKugel(r);
		
		//Ausgabe
		System.out.printf("Das Volumen der Kugel betr�gt: %.2f\n ", ergebnisVolumenKugel);

	}
	public static double berechnungVolumenKugel(double zahl1) {
		double ergebnisVolumenKugel = 4/3 * zahl1 * zahl1 * zahl1 * 3.1415;
		return ergebnisVolumenKugel;
	}

}
