package temp;

public class Temperatur {
	
	//Attribute
	private Datum datum;
	private double temp6Uhr;
	private double temp12Uhr;
	private double temp18Uhr;
	private double temp0Uhr;
	
	
	
	//Konstruktor
	public Temperatur() {
		this.datum = new Datum();
	}
	//Verwaltungsmethoden
	public double minTemperatur(){
		if(temp0Uhr < temp6Uhr && temp0Uhr <= temp12Uhr && temp0Uhr <= temp18Uhr) return this.temp0Uhr;
		if(temp6Uhr < temp0Uhr && temp6Uhr <= temp12Uhr && temp6Uhr <= temp18Uhr) return this.temp6Uhr;
		if(temp12Uhr < temp6Uhr && temp12Uhr <= temp0Uhr && temp12Uhr <= temp18Uhr) return this.temp12Uhr;
		return this.temp18Uhr;
	}
	public double maxTemperatur() {
		if(temp0Uhr > temp6Uhr && temp0Uhr >= temp12Uhr && temp0Uhr >= temp18Uhr) return this.temp0Uhr;
		if(temp6Uhr > temp0Uhr && temp6Uhr >= temp12Uhr && temp6Uhr >= temp18Uhr) return this.temp6Uhr;
		if(temp12Uhr > temp6Uhr && temp12Uhr >= temp0Uhr && temp12Uhr >= temp18Uhr)return this.temp12Uhr;
		return this.temp18Uhr;
	}
	public double getTemp0Uhr() {
		return temp0Uhr;
	}
	public void setTemp0Uhr(double temp) {
		this.temp0Uhr = temp;
	}
	public double getTemp6Uhr() {
		return temp6Uhr;
	}
	public void setTemp6Uhr(double temp) {
		this.temp6Uhr = temp;
	}
	public double getTemp12Uhr() {
		return temp12Uhr;
	}
	public void setTemp12Uhr(double temp) {
		this.temp12Uhr = temp;
	}
	public double getTemp18Uhr() {
		return temp18Uhr;
	}
	public void setTemp18Uhr(double temp) {
		this.temp18Uhr = temp;
	}
}






