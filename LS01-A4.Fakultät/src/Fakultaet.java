import java.util.Scanner;
public class Fakultaet {

	public static void main(String[] args) {
		int zaehler = 1;
		int erg =1;
		int n;
		
		System.out.print("Geben Sie Ihre Zahl ein: ");
		Scanner tastatur = new Scanner(System.in);
		n = tastatur.nextInt();
		
		
		while (zaehler <= n) {
			System.out.print(zaehler);
			if (zaehler != n) {
				System.out.print(" * ");
			}
			erg = erg * zaehler;
			zaehler = zaehler + 1;
			
		}
		
		System.out.println(" = " + erg);
	}
}
