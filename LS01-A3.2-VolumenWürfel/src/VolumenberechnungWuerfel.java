import java.util.Scanner;
public class VolumenberechnungWuerfel {

	public static void main(String[] args) {

		double a;
		double ergebnisVolumenWuerfel;
		Scanner tastatur = new Scanner(System.in);
		
		
		//Eingabe
		System.out.println("Bitte geben Sie Seite a ein: ");
		a = tastatur.nextDouble();
		
		//Verarbeitung
		ergebnisVolumenWuerfel = berechneVolumenWuerfel(a);
		
		//Ausgabe
		System.out.printf("Das Volumen des W�rferls betr�gt: %.2f\n ",ergebnisVolumenWuerfel);

	}

	public static double berechneVolumenWuerfel(double zahl1) {
		double ergebnisVolumenWuerfel = zahl1 * zahl1* zahl1;
		return ergebnisVolumenWuerfel;
	}
	
}
