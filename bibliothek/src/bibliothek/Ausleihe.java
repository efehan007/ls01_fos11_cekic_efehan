package bibliothek;

public class Ausleihe {

	//Attribute
	private int ausleihnummer;
	private Buch buch;
	private Person person;
	
	//Konstruktor
	public Ausleihe() {
		this.ausleihnummer = 0;
		this.buch = new Buch();
		this.person = new Person();
	}
	//Verwaltungsmethoden
	public int getAusleihnummer() {
		return this.ausleihnummer;
	}
	public void setAusleihnummer(int ausleihnummer) {
		this.ausleihnummer = ausleihnummer;
	}
	public Buch getBuch() {
		return this.buch;
	}
	public void setBuch(Buch buch) {
		this.buch = buch;
	}
	public Person getPerson() {
		return this.person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String toString() {
	return "Ausleihnummer: " + this.ausleihnummer + "Buch: " + this.buch + "Person: " + this.person;
	}
}

