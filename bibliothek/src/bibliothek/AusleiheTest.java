package bibliothek;

public class AusleiheTest {

	public static void main(String[] args) {
		Ausleihe ausleihe1 = new Ausleihe();
		
		ausleihe1.setAusleihnummer(3738);
		Buch b1 = new Buch();
		b1.setTitel("Harry Potter");
		b1.setAutor("Joanne Rowling");
		ausleihe1.setBuch(b1);
		
		Person p1 = new Person();
		p1.setKundennummer("76374621");
		p1.setVorname("Efehan");
		p1.setNachname("Cekic");
		ausleihe1.setPerson(p1);
		
		Adresse a1 = new Adresse();
		a1.setStraße("Goldhähnchenweg 19");
		a1.setPlz(12359);
		a1.setOrt("Berlin");
		p1.setAnschrift(a1);
		
		System.out.println(ausleihe1);

	}

}

