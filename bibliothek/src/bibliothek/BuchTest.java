package bibliothek;



public class BuchTest {

	public static void main(String[] args) {
		Buch b1 = new Buch();
		b1.setTitel("Harry Potter");
		b1.setAutor("Joanne Rowling");
		
		Buch b2 = new Buch();
		b2.setTitel("Bibel");
		b2.setAutor("Apostel Johannes");
		
		Buch b3 = new Buch();
		b3.setTitel("Bibel des Spaghettimonsters");
		b3.setAutor("Bobby Henderson");
		
		Buch b4 = new Buch();
		b4.setTitel("Duden");
		b4.setAutor("Konrad Duden");
		
		Buch b5 = new Buch();
		b5.setTitel("1453: Die letzte gro�e Eroberung");
		b5.setAutor("Roger Crowley");

		System.out.println(b1);
		System.out.println(b2);
		System.out.println(b3);
		System.out.println(b4);
		System.out.println(b5);
		
	}
}
