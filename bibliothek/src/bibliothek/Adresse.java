package bibliothek;

public class Adresse {

	//Attribute
	private String stra�e;
	private int plz;
	private String ort;
	
	//Konstruktor
	public Adresse() {
		this.stra�e = "unbekannt";
		this.plz = 0;
		this.ort = "unbekannt";
	}
	
	//Verwaltungsmethoden
	public String getStra�e() {
		return this.stra�e;
	}
	public void setStra�e(String stra�e) {
		this.stra�e = stra�e;
	}
	public int getPlz() {
		return this.plz;
	}
	public void setPlz(int plz) {
		this.plz = plz;
	}
	public String getOrt() {
		return this.ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	public String toString() {
		return "Stra�e: " + this.stra�e + "; Postleitzahl: " + this.plz + "; Ort: " + this.ort;
	}
}
