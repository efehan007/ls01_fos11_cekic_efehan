package bibliothek;

public class AdresseTest {

	public static void main(String[] args) {
		Adresse a1 = new Adresse();
		a1.setStraße("Goldhähnchenweg 19");
		a1.setPlz(12359);
		a1.setOrt("Berlin");
		
		Adresse a2 = new Adresse();
		a2.setStraße("Frankfurter Allee 80");
		a2.setPlz(10247);
		a2.setOrt("Berlin");
		
		Adresse a3 = new Adresse();
		a3.setStraße("Haarlemer Str. 27");
		a3.setPlz(12359);
		a3.setOrt("Berlin");
		
		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a3);
	}

}
