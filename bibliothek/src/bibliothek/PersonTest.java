package bibliothek;

public class PersonTest {

	public static void main(String[] args) {
		Person p1 = new Person();
		p1.setKundennummer("76374621");
		p1.setVorname("Efehan");
		p1.setNachname("Cekic");
		
		Adresse a1 = new Adresse();
		a1.setStraße("Goldhähnchenweg 19");
		a1.setPlz(12359);
		a1.setOrt("Berlin");
		p1.setAnschrift(a1);
		
		
		Person p2 = new Person();
		p2.setKundennummer("834324532");
		p2.setVorname("Niklas");
		p2.setNachname("Oppermann");
		
		Adresse a2 = new Adresse();
		a2.setStraße("Frankfurter Allee 80");
		a2.setPlz(10247);
		a2.setOrt("Berlin");
		p2.setAnschrift(a2);
		
		Person p3 = new Person();
		p3.setKundennummer("6666666666");
		p3.setVorname("Salim");
		p3.setNachname("Gereev");
		
		Adresse a3 = new Adresse();
		a3.setStraße("Haarlemer Str. 27");
		a3.setPlz(12359);
		a3.setOrt("Berlin");
		p3.setAnschrift(a3);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		

	}

}
