import java.util.Scanner;
public class Volumenberechnung {

	public static void main(String[] args) {
		double a;
		double b;
		double c;
		double h;
		double r;
		double ergebnisVolumenWuerfel;
		double ergebnisVolumenQuader;
		double ergebnisVolumenPyramide;
		double ergebnisVolumenKugel;
		
		//Eingabe
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die Seite a ein: ");
		a = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie die Seite b ein: ");
		b = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie die Seite c ein: ");
		c = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie die H�he ein: ");
		h = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie den Radius ein: ");
		r = tastatur.nextDouble();
		
		//Verarbeitung
		ergebnisVolumenWuerfel = berechneVolumenWuerfel(a);
		ergebnisVolumenQuader = berechneVolumenQuader(a,b,c);
		ergebnisVolumenPyramide = berechneVolumenPyramide(a,h);
		ergebnisVolumenKugel = berechneVolumenKugel(r);
		
		
		
		//Ausgabe
		System.out.printf("Das Volumen vom W�rfel ist: %.2f\n", ergebnisVolumenWuerfel);
		System.out.printf("Das Volumen vom Quader ist: %.2f\n", ergebnisVolumenQuader);
		System.out.printf("Das Volumen der Pyramide ist: %.2f\n", ergebnisVolumenPyramide);
		System.out.printf("Das Volumen der Kugel betr�gt: %.2f\n", ergebnisVolumenKugel);
	}
	//Volumenberechnung von W�rfel
	public static double berechneVolumenWuerfel(double zahl1) {
		double ergebnisVolumenWuerfel = zahl1 * zahl1 * zahl1;
		return ergebnisVolumenWuerfel;
	}
	//Volumenberechnung von Quader
	public static double berechneVolumenQuader(double zahl1, double zahl2, double zahl3) {
		double ergebnisVolumenQuader = zahl1 * zahl2 * zahl3;
		return ergebnisVolumenQuader;
	}
	//Volumenberechnung von Pyramide
	public static double berechneVolumenPyramide(double zahl1, double zahl4) {
		double ergebnisVolumenPyramide = zahl1 * zahl1 * zahl4 /3;
		return ergebnisVolumenPyramide;
	}
	//Volumenberechnung von Kugel
	public static double berechneVolumenKugel(double zahl5) {
		double ergebnisVolumenKugel = 4/3 * zahl5 * zahl5 * zahl5 * 3.1415;
		return ergebnisVolumenKugel;
		
	}
	
}
