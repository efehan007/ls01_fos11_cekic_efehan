import java.util.Scanner;

public class VolumenberechnungQuader {

	public static void main(String[] args) {
		
		double a;
		double b;
		double c;
		double ergebnisVolumenQuader;
		
		//Eingabe
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie Seite a ein: ");
		a = tastatur.nextDouble();
		
		System.out.print("Bitte geben Sie Seite b ein: ");
		b = tastatur.nextDouble();
		
		System.out.print("Bitte geben Sie Seite c ein: ");
		c = tastatur.nextDouble();
		
		//Verarbeitung
		ergebnisVolumenQuader = berechneVolumenQuader(a,b,c);
		
		//Ausgabe
		System.out.printf("Das Volumen des Quaders betr�gt: %.2f\n ", ergebnisVolumenQuader);
		
	

	}
	
	public static double berechneVolumenQuader(double zahl1, double zahl2, double zahl3) {
		double berechneVolumenQuader = zahl1 * zahl2 * zahl3;
		return berechneVolumenQuader;
	}

	
	
}
