
public class Bruch {
	//Attribute
	private int zaehler;
	private int nenner;
	
	//Konstruktoren
	public Bruch(int zaehler, int nenner) {
		this.nenner = nenner;
		this.zaehler = zaehler;
	}
	public int getZaehler() {
		return zaehler;
	}
	public void setZaehler(int zahl) {
		this.zaehler = zaehler;
	}
	public int getNenner() {
		return nenner;
	}
	public void setNenner() {
		this.nenner = nenner;
	}
}
