
public class Feiertag extends Datum{
	//Attribute
	private String feiertagsname;
	//Konstruktoren
	
	public Feiertag(int tag, int monat, int jahr, String feiertagsname) {
		super(tag, monat, jahr);
		this.feiertagsname = feiertagsname;
	}
	//Verwaltungsmethoden
	public String getFeiertagsname() {
		return feiertagsname;
	}
	public void setFeiertagsname(String feiertagsname) {
		this.feiertagsname = feiertagsname;
	}
	@Override
	public String toString(){
	    return "Datum: " + this.getTag() + "." + this.getMonat() + "."+ this.getJahr() + " "  +"Feiertag: " + this.getFeiertagsname();
	}
}
