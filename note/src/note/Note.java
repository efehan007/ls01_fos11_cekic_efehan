package note;

public class Note {
	//Attribute
	private int notenpunkte;
	private String leistung;
	
	
	//Konstruktor
	public Note(int notenpunkte, String leistung) {
		this.notenpunkte = notenpunkte;
		this.leistung = leistung;
	}
	public int getNotenpunkt() {
		return notenpunkte;
	}
	public void setNotenpunkte(int notenpunkte) {
		this.notenpunkte = notenpunkte;
	}
	public String getLeistung() {
		return leistung;
	}
}
