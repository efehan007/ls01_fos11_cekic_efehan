package note;

public class Fach {
	//Attribute
	private String name;
	private Note[] noten = new Note[20];
	private int zahl = 0;
	
	//Konstruktor
	public Fach(String name) {
		this.name = name;
	}
	//Verwaltungsmethoden
	public void vergebeNoten(Note noten) {
		  this.noten[zahl] = noten;
		  zahl++;
	}
	public void NoteDiskutieren(int zahl, Note neueNote) {
		this.noten[zahl] = neueNote;
	}
	public double durchschnitt() {
		int summe = noten[0].getNotenpunkt();
		for(int i = 1;i<noten.length; i++) {
			summe = summe + noten[i].getNotenpunkt();
		}
		return summe/noten.length;
	}
	public String getName() {
		return name;
	}
	public Note[] getNoten() {
		return noten;
	}
}
