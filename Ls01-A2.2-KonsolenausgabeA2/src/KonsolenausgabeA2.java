

import java.util.Scanner;
public class KonsolenausgabeA2 {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Guten Tag");
		System.out.println("Wie hei�en Sie?");
		
		String name = myScanner.nextLine();
		
		System.out.println("Wie alt sind Sie?");
		
		int alter = myScanner.nextInt();
		
		System.out.println("Hallo " + name + ", sie sind " + alter + " Jahre alt.");

		myScanner.close();
	}

}
