package implentierung;

public class Schueler extends Person{
	//Attribute
	private int schuelerNr;
	private String klasse;
	
	//Konstruktor
	public Schueler() {
		super();
		this.schuelerNr = 0;
		this.klasse = "unbekannt";
	}
	public Schueler(String name) {
		super(name);
		this.klasse = "unbekannt";
	}
	public Schueler(String name, String klasse) {
		super(name);
		this.klasse = klasse;
	}
	//Verwaltungsmothoden
	public int getSchuelerNr() {
		return this.schuelerNr;
	}
	public void setSchuelerNr(int schuelerNr) {
		this.schuelerNr = schuelerNr;
	}
	public String getKlasse() {
		return this.klasse;
	}
	public void setKlasse(String klasse) {
		this.klasse = klasse; 
	}
	public String toString() {
		return "Sch�lerNr: " + this.schuelerNr + "; Klasse: " + this.klasse;
	}
}
