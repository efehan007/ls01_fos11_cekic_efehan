package implentierung;

public class Person {
	//Attribute
	private String name;
	
	//Konstruktor
	public Person() {
		this.name = "unbekannt";
	}
	public Person(String name) {
		this.name = name;
		
		
	}
	//Verwaltungsmethoden
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
