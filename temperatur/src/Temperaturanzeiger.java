//package temperatur;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Temperaturanzeiger extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2234100680294456773L;
	private JLabel lbl_Datum;
	private JLabel lblMaxtemp;
	private JLabel lblMintemp;
	private JLabel lblAvgtemp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Temperaturanzeiger frame = new Temperaturanzeiger();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Temperaturanzeiger() {
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Temperaturanzeige");
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Stencil", Font.BOLD, 23));
		getContentPane().add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		lbl_Datum = new JLabel("01.01.1970");
		lbl_Datum.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lbl_Datum.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_Datum, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblMaximaleTemperatur = new JLabel("maximale Temperatur");
		lblMaximaleTemperatur.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblMaximaleTemperatur);
		
		lblMaxtemp = new JLabel("maxTemp");
		lblMaxtemp.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(lblMaxtemp);
		
		JLabel lblMinimaleTemperatur = new JLabel("minimale Temperatur");
		lblMinimaleTemperatur.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblMinimaleTemperatur);
		
		lblMintemp = new JLabel("minTemp");
		lblMintemp.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(lblMintemp);
		
		JLabel lblDurchschnittlicheTemperatur = new JLabel("durchschnittliche Temperatur");
		lblDurchschnittlicheTemperatur.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblDurchschnittlicheTemperatur);
		
		lblAvgtemp = new JLabel("avgTemp");
		lblAvgtemp.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(lblAvgtemp);
		
		JButton btnNewButton = new JButton("Temperatur anzeigen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Temperatur temp = new Temperatur();
				temp.setDatum(new Datum(18,2,2021));
				temp.setTemp0Uhr(-7.0);
				temp.setTemp6Uhr(-8);
				temp.setTemp12Uhr(45.7);
				temp.setTemp18Uhr(80);
				
				lbl_Datum.setText(temp.getDatum().toString());
				lblMaxtemp.setText(temp.maxTemperatur() + "�C");
				lblMintemp.setText("" + temp.minTemperatur()+ "�C");
				lblAvgtemp.setText("" + temp.durchschnittlicheTemperatur()+ "�C");
			}
		});
		panel.add(btnNewButton, BorderLayout.SOUTH);

	}

}
