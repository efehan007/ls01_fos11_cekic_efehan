//package temperatur;


public class Datum {

	private int tag, monat, jahr;

	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) {
		this.tag = tag;
		this.monat = monat;
		this.jahr = jahr;
	}

	public String monatAlsString() {
		switch(this.monat) {
		case 1: return "Januar";
		case 2: return "Februar";
		case 3: return "M�rz";
		case 4: return "April";
		case 5: return "Mai";
		case 6: return "Juni";
		case 7: return "Juli";
		case 8: return "August";
		case 9: return "September";
		case 10: return "Oktober";
		case 11: return "November";
		case 12: return "Dezember";
		default: return "nicht vorhandener Monat";
		}
	}
	
	@Override
	public String toString() {
		return tag + "." + monatAlsString() + "." + jahr;
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}
	
	

}
