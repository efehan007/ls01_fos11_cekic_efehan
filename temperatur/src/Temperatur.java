//package temperatur;


public class Temperatur {

	private Datum datum;
	private double[] tempmessungen = new double[24];
 	
	public Temperatur() {
		this.datum = new Datum();
	}
	
	public double minTemperatur() {
		double minimum = tempmessungen[0];
		for(int i = 1; i<tempmessungen.length; i++) {
			if (minimum > tempmessungen[i]) {
				minimum = tempmessungen[i];
			}
		}
		return minimum;
		
	}

	public double maxTemperatur() {
		double maximum = tempmessungen[0];
		for (int i = 1; i < tempmessungen.length; i++) {
			if (maximum < tempmessungen[i]) {
				maximum = tempmessungen[i];
			}
		}
		return maximum;
	}
	
	public double durchschnittlicheTemperatur() {
		double summe = tempmessungen[0];
		for (int i=1; i<tempmessungen.length; i++) {
			summe = summe + tempmessungen[i];
		}
		return summe /tempmessungen.length;
	}

	public Datum getDatum() {
		return datum;
	}

	public void setDatum(Datum datum) {
		this.datum = datum;
	}
}
