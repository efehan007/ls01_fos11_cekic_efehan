import java.util.Scanner;

public class VolumenberechnungPyramide {

	public static void main(String[] args) {
		
		double a;
		double h;
		double ergebnisBerechnungPyramide;
		
		//Eingabe
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie Seite a ein: ");
		a = tastatur.nextDouble();
		
		System.out.print("Bitte geben Sie die H�he ein: ");
		h = tastatur.nextDouble();
		
		//Verarbeitung
		ergebnisBerechnungPyramide = berechnungVolumenPyramide(a,h);
		
		//Ausgabe 
		System.out.printf("Das Volumen der Pyramide betr�gt: %.2f\n ", ergebnisBerechnungPyramide);
		

	}
	public static double berechnungVolumenPyramide(double zahl1, double zahl2) {
		double ergebnisBerechnungPyramide = zahl1 * zahl1 * zahl2/3;
		return ergebnisBerechnungPyramide;
	}
}
