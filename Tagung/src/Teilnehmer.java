
public class Teilnehmer extends Person {
	//Attribute
	private String status;
	
	//Konstruktor
	public Teilnehmer() {
	}
	public Teilnehmer(String nachname, String vorname, String status) {
		super(nachname, vorname);
		this.status = status;
	}
	//Verwaltungsmethoden
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return this.status;
	}
	public String toString() {
		return String.format("Person   | %20s | %20s | Status: %s%n", this.getNachname(), this.getVorname(), this.getStatus());
	}
}
