package drucuck;

public class Briefkopf {
	private Adresse absender;
	private Adresse empfaenger;
	private String datum;
	private String betreff;
	
	public Briefkopf(Adresse absender, Adresse empfaenger, String datum, String betreff ) {
		this.absender = absender;
		this.empfaenger = empfaenger;
		this.datum = datum;
		this.betreff = betreff;
	}
	public Adresse getAbsender() {
		return absender;
	}
	public void setAbsender(Adresse absender) {
		this.absender = absender;
	}
	public Adresse getEmpfaenger() {
		return empfaenger;
	}
	public void setEmpfaenger(Adresse empfaenger) {
		this.empfaenger = empfaenger;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	public String getBetreff() {
		return betreff;
	}
	public void setBetreff(String betreff) {
		this.betreff = betreff;
	}
	public void drucken() {
			//Druckauftag beantragen
			PrinterJob pjob = PrinterJob.getPrinterJob();
			if (pjob.printDialog() == false)
			return;
			//Druckauftrag f�llen - eine Klasse Printable wird ben�tigt
			pjob.setPrintable(new Printable() {
			public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
			//Anzahl der Seiten
			if (pageIndex >= 1)
			return Printable.NO_SUCH_PAGE;
			//Layout (Zeilenabstand)
			int zeile = 5, fontsize = 12;
			//Schriftart setzen
			g.setFont(new Font("Arial", Font.PLAIN, fontsize));
			//auf die Druckerseite zeichnen
			g.drawString(absender.getAnrede(), 60, zeile++ * (fontsize+2));
			g.drawString(absender.getVorname() + " " + absender.getNachname(), 60, zeile++ * (fontsize+2));
			g.drawString(absender.getAdresszusatz(), 60, zeile++ * (fontsize+2));
			g.drawString(absender.getStrasse() + " " + absender.getHausnummer(), 60, zeile++ * (fontsize+2));
			g.drawString(absender.getLand()+absender.getPlz()+" "+absender.getOrt(),60,zeile++*(fontsize+2));
			zeile +=3;
			g.setFont(new Font("Arial", Font.BOLD, fontsize));
			g.drawString(empfaenger.getAnrede(), 60, zeile++ * (fontsize+2));
			g.drawString(empfaenger.getVorname()+" "+empfaenger.getNachname(), 60, zeile++ * (fontsize+2));
			g.drawString(empfaenger.getAdresszusatz(), 60, zeile++ * (fontsize+2));
			g.drawString(empfaenger.getStrasse()+" "+empfaenger.getHausnummer(), 60, zeile++ * (fontsize+2));
			g.drawString(empfaenger.getLand()+empfaenger.getPlz()+" "+empfaenger.getOrt(),60,zeile++*(fontsize+2));
			zeile +=2;
			g.drawString(betreff + " vom " + datum, 60, zeile * (fontsize+2));
			//Seite best�tigen
			return Printable.PAGE_EXISTS;
			}
			});
			//Druckauftrag abschicken
			try {
			pjob.print();
			} catch (PrinterException e) {
			System.out.println("Auftrag konnte nicht gedruckt werden");
			e.printStackTrace();
			}
			}

	}
}
