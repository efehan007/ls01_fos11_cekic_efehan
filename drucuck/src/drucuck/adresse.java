package drucuck;

public class Adresse {
	private String anrede;
	private String vorname;
	private String nachname;
	private String strasse;
	private String hausnummer;
	private String adresszusatz;
	private String land;
	private String plz;
	private String ort;
	
	public Adresse(String anrede, String vorname, String nachname, String strasse, String hausnummer, String adresszusatz, String land, String plz, String ort) {
		this.anrede = anrede;
		this.vorname = vorname;
		this.nachname = nachname;
		this.strasse = strasse;
		this.hausnummer = hausnummer;
		this.adresszusatz = adresszusatz;
		this.land = land;
		this.plz = plz;
		this.ort = ort;
	}
	public String getAnrede() {
		return anrede;
	}
	public void setAnrede(String anrede) {
		this.anrede = anrede;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getStrasse() {
		return strasse;
	}
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	
}
