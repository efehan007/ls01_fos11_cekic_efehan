import java.util.Scanner;
public class Multiplizieren {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		double wert1;
		double wert2;
		double ergebnis;
		
		
		//Eingabe
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		wert1 = tastatur.nextDouble();
		System.out.println("Bitte geben Sie noch eine Zahl ein: ");
		wert2 = tastatur.nextDouble();
		
		
		//Verarbeitung
		ergebnis = multiplikation(wert1, wert2);
		
		
		//Ausgabe
		System.out.printf("Das Ergebnis lautet: %.2f\n", ergebnis);
		tastatur.close();
	}
	
	public static double multiplikation(double a, double b) {
		
		double ergebnis = a * b;
		return ergebnis;		
		
		
		
		
	}

}
